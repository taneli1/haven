// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { bool, object, func, string } from 'prop-types'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'
import { Button, Fieldset, TextInput } from '@commonground/design-system'

import { Form } from './index.styles'

const DEFAULT_INITIAL_VALUES = {
  apiVersion: 'helm.toolkit.fluxcd.io/v2beta1',
  kind: 'HelmRelease',
  metadata: {
    name: 'hallo-wereld',
    namespace: 'default',
  },
  spec: {
    interval: '5m',
    chart: {
      spec: {
        chart: 'hallo-wereld',
        version: '0.1.0',
        sourceRef: {
          kind: 'HelmRepository',
          name: 'commonground',
          namespace: 'default',
        },
        interval: '1m',
      },
    },
    values: {},
  },
}

const validationSchema = Yup.object().shape({
  metadata: Yup.object().shape({
    name: Yup.string().required(),
    namespace: Yup.string().required(),
  }),
})

const ReleaseForm = ({
  initialValues,
  disableForm,
  onSubmitHandler,
  submitButtonText,
}) => {
  const { t } = useTranslation()

  return (
    <Formik
      initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
      validationSchema={validationSchema}
      onSubmit={onSubmitHandler}
    >
      {({ values, handleSubmit }) => (
        <Form onSubmit={handleSubmit}>
          <Fieldset>
            <TextInput name="metadata.name" size="l" disabled={!!initialValues}>
              {t('Name')}
            </TextInput>
            <TextInput
              name="metadata.namespace"
              size="l"
              disabled={!!initialValues}
            >
              {t('Namespace')}
            </TextInput>
          </Fieldset>

          <Fieldset>
            <TextInput name="spec.chart.spec.chart" size="l">
              {t('Chart')}
            </TextInput>

            <TextInput name="spec.chart.spec.version" size="l">
              {t('Version')}
            </TextInput>

            <TextInput name="spec.chart.spec.sourceRef.name" size="l">
              {t('Repository name')}
            </TextInput>

            <TextInput name="spec.chart.spec.sourceRef.namespace" size="l">
              {t('Repository namespace')}
            </TextInput>
          </Fieldset>

          <Button type="submit" disabled={disableForm}>
            {submitButtonText}
          </Button>
        </Form>
      )}
    </Formik>
  )
}

ReleaseForm.propTypes = {
  initialValues: object,
  disableForm: bool,
  onSubmitHandler: func,
  submitButtonText: string,
}

export default ReleaseForm
