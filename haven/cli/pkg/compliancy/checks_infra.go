// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"regexp"
	"strings"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func infraMultiMaster(config *Config) (Result, error) {
	// Amazon EKS, Azure AKS and Google cloud GKE control plane does not appear in the nodes list and
	// is high available by default
	if config.HostPlatform == PlatformEKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformAKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformGKE {
		return ResultYes, nil
	}

	nodes, err := config.KubeClient.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	masterCount := 0
	for _, n := range nodes.Items {
		if n.Labels["kubernetes.io/role"] == "master" {
			masterCount++
			continue
		}

		if _, exists := n.Labels["node-role.kubernetes.io/control-plane"]; exists {
			masterCount++
			continue
		}

		if _, exists := n.Labels["node-role.kubernetes.io/master"]; exists {
			masterCount++
			continue
		}

		if _, exists := n.Labels["node.kubernetes.io/master"]; exists {
			masterCount++
			continue
		}
	}

	if masterCount >= 3 {
		return ResultYes, nil
	}

	if masterCount == 0 {
		return ResultUnknown, nil
	}

	return ResultNo, nil
}

func infraMultiWorker(config *Config) (Result, error) {
	nodes, err := config.KubeClient.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})

	if err != nil {
		return ResultNo, err
	}

	workerCount := 0
	for _, n := range nodes.Items {
		if "master" != n.Labels["kubernetes.io/role"] {
			workerCount++
		}

		if workerCount >= 3 {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}

func infraSecuredNodes(config *Config) (Result, error) {
	// OpenShift uses Red Hat Enterprise Linux CoreOS, an immutable operating system
	// especially for container platforms. As a platform feature, it always has
	// SELinux enabled. On managed services like ARO, this can be manually
	// verified through `oc debug node/$master`, there is no SSH access.
	if config.HostPlatform == PlatformOpenShift {
		return ResultYes, nil
	}

	logging.Info("Check node security: Using fallback method with privileged pod for 10-40 seconds.\n")

	// AppArmor.
	out, err := RunCommandOnPrivilegedPod(config, false, false, "cat /sys/module/apparmor/parameters/enabled")
	if err != nil {
		return ResultUnknown, err
	}

	if strings.Contains(out, "Y") {
		return ResultYes, nil
	}

	// SELinux.
	out, err = RunCommandOnPrivilegedPod(config, false, false, "sestatus")
	if err != nil {
		return ResultUnknown, err
	}

	re := regexp.MustCompile(`SELinux status:([ ]+)enabled`)
	if re.MatchString(out) {
		return ResultYes, nil
	}

	// Grsecurity.
	out, err = RunCommandOnPrivilegedPod(config, false, true, "uname -a")
	if err != nil {
		return ResultUnknown, err
	}

	if strings.Contains(out, "grsec") {
		return ResultYes, nil
	}

	// LKRG.
	out, err = RunCommandOnPrivilegedPod(config, false, true, "lsmod")
	if err != nil {
		return ResultUnknown, err
	}

	if strings.Contains(out, "p_lkrg") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func infraPrivateTopology(config *Config) (Result, error) {
	nodes, err := config.KubeClient.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, n := range nodes.Items {
		for _, a := range n.Status.Addresses {
			if "ExternalIP" == a.Type && "" != a.Address {
				return ResultNo, nil
			}
		}
	}

	return ResultYes, nil
}

func infraMultiAZ(config *Config) (Result, error) {
	nodes, err := config.KubeClient.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	zone := ""
	zoneCount := 0

	for _, n := range nodes.Items {
		curZone := n.Labels[apiv1.LabelZoneFailureDomainStable]

		if zone != curZone {
			zone = curZone
			zoneCount++
		}

		if zoneCount > 1 {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}
