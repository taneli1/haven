package crd

import (
	"context"
	"time"

	rbacapi "k8s.io/api/rbac/v1"
	apiextensionv1beta1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	"k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	rbachelper "k8s.io/kubernetes/pkg/apis/rbac/v1"
)

const (
	GroupName string = "haven.commonground.nl"
	Kind      string = "Compliancy"
	Version   string = "v1alpha1"
	Singular  string = "compliancy"
	ShortName string = "compliancy"
	Plural    string = "compliancies"
	Name      string = Plural + "." + GroupName
)

// CreateOrRetrieveCRD retrieves the Haven CRD and will deploy it to the cluster when it's not installed yet.
func CreateOrRetrieveCRD(client *kubernetes.Clientset, apiextensionsclientset *clientset.Clientset) (bool, error) {
	created := false

	if !exists(apiextensionsclientset) {
		err := create(client, apiextensionsclientset)
		if err != nil {
			return false, err
		}

		created = true
	}

	return created, nil
}

func exists(apiextensionsclientset *clientset.Clientset) bool {
	_, err := apiextensionsclientset.ApiextensionsV1().CustomResourceDefinitions().Get(context.Background(), "compliancies.haven.commonground.nl", metav1.GetOptions{})

	return err == nil
}

func waitAccepted(apiextensionsclientset *clientset.Clientset) error {
	err := wait.Poll(1*time.Second, 30*time.Second, func() (bool, error) {
		crd, err := apiextensionsclientset.ApiextensionsV1beta1().CustomResourceDefinitions().Get(context.TODO(), Name, metav1.GetOptions{})

		if err != nil {
			return false, err
		}

		for _, condition := range crd.Status.Conditions {
			if condition.Type == apiextensionv1beta1.Established && condition.Status == apiextensionv1beta1.ConditionTrue {
				return true, nil
			}
		}

		return false, nil
	})

	return err
}

func create(client *kubernetes.Clientset, apiextensionsclientset *clientset.Clientset) error {
	crd := &apiextensionv1beta1.CustomResourceDefinition{
		ObjectMeta: metav1.ObjectMeta{Name: Name},
		Spec: apiextensionv1beta1.CustomResourceDefinitionSpec{
			Group:   GroupName,
			Version: Version,
			Scope:   apiextensionv1beta1.ClusterScoped,
			Names: apiextensionv1beta1.CustomResourceDefinitionNames{
				Plural:     Plural,
				Singular:   Singular,
				ShortNames: []string{ShortName},
				Kind:       Kind,
			},
			Validation: &apiextensionv1beta1.CustomResourceValidation{
				OpenAPIV3Schema: &apiextensionv1beta1.JSONSchemaProps{
					Type: "object",
					Properties: map[string]apiextensionv1beta1.JSONSchemaProps{
						"spec": {
							Type: "object",
							Properties: map[string]apiextensionv1beta1.JSONSchemaProps{
								"created":   {Type: "string"},
								"version":   {Type: "string"},
								"compliant": {Type: "boolean"},
								"output":    {Type: "string"},
							},
							Required: []string{
								"created",
								"version",
								"compliant",
							},
						},
					},
				},
			},
			AdditionalPrinterColumns: []apiextensionv1beta1.CustomResourceColumnDefinition{
				{
					Name:     "version",
					Type:     "string",
					JSONPath: ".spec.version",
				},
				{
					Name:     "compliant",
					Type:     "boolean",
					JSONPath: ".spec.compliant",
				},
			},
		},
	}

	_, err := apiextensionsclientset.ApiextensionsV1beta1().CustomResourceDefinitions().Create(context.Background(), crd, metav1.CreateOptions{})
	if err != nil {
		return err
	}

	if err := waitAccepted(apiextensionsclientset); err != nil {
		return err
	}

	clusterrole := &rbacapi.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{Name: "haven-compliancy-view"},
		Rules: []rbacapi.PolicyRule{
			rbachelper.NewRule("get", "list", "watch").Groups("haven.commonground.nl").Resources("compliancies").RuleOrDie(),
		},
	}

	clusterrolebinding := &rbacapi.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{Name: "haven-compliancy-view-authenticated"},
		RoleRef:    rbacapi.RoleRef{Kind: "ClusterRole", Name: "haven-compliancy-view"},
		Subjects: []rbacapi.Subject{
			{Kind: "Group", Name: "system:authenticated"},
		},
	}

	if _, err = client.RbacV1().ClusterRoles().Create(context.TODO(), clusterrole, metav1.CreateOptions{}); err != nil {
		return err
	}

	if _, err = client.RbacV1().ClusterRoleBindings().Create(context.TODO(), clusterrolebinding, metav1.CreateOptions{}); err != nil {
		return err
	}

	return err
}
