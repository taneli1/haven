# Haven dashboard

This folder should contain a build of the Haven dashboard and will be included in the Haven CLI binary. To manually build the dashboard use:

```bash
(cd haven/dashboard && npm install && npm run build)
(cp -R haven/dashboard/build/* haven/cli/static/haven-dashboard)
```