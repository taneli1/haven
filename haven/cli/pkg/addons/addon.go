// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	"context"
	"io/ioutil"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/pkg/errors"
	"github.com/pytimer/k8sutil/apply"
	"golang.org/x/crypto/bcrypt"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
	"sigs.k8s.io/yaml"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/connect"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/rand"
)

// Addon is installable on a Haven cluster
type Addon struct {
	Name              string
	Namespace         string
	Description       string
	Questions         []Question
	GeneratedValues   []GeneratedValue
	Chart             Chart
	DefaultValuesFile string
	PreInstallYaml    []InstallYaml
	PostInstallYaml   []InstallYaml
}

// GetSurveyQuestions returns questions in the form that survey expects
func (a *Addon) GetSurveyQuestions() []*survey.Question {
	surveyQuestions := []*survey.Question{}

	for _, question := range a.Questions {
		var defaultValue string
		if question.Default.RandString > 0 {
			defaultValue = rand.String(question.Default.RandString)
		}
		if question.Default.String != "" {
			defaultValue = question.Default.String
		}

		var transform func(s interface{}) interface{}
		if question.Transform.Password == "bcrypt" {
			transform = func(s interface{}) interface{} {
				inputString, ok := s.(string)
				if !ok {
					return ""
				}

				b, err := bcrypt.GenerateFromPassword([]byte(inputString), bcrypt.DefaultCost)
				if err != nil {
					return ""
				}

				return string(b)
			}
		}

		surveyQuestions = append(surveyQuestions, &survey.Question{
			Name: question.Name,
			Prompt: &survey.Input{
				Message: question.Message,
				Help:    question.Help,
				Default: defaultValue,
			},
			Transform: transform,
		})
	}

	return surveyQuestions
}

// GetValues opens the DefaultValuesFile, performs a search-and-replace for customValues and unmarshals the contents
func (a *Addon) GetValues(customValues map[string]interface{}) (map[string]interface{}, error) {
	f, err := embedFiles.Open(a.DefaultValuesFile)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	s := string(b)

	for key, value := range customValues {
		newValue, ok := value.(string)
		if !ok {
			continue
		}

		s = strings.ReplaceAll(s, key, newValue)
	}

	values := make(map[string]interface{})

	err = yaml.Unmarshal([]byte(s), &values)
	if err != nil {
		return nil, err
	}

	return values, nil
}

// ApplyYaml applies embedded yaml from pre- and post install addon configuration,
// taking custom values from survey questions into account.
func ApplyYaml(installyaml []InstallYaml, customValues map[string]interface{}) error {
	kubeConfig, err := connect.K8s()
	if err != nil {
		return errors.Errorf("Could not connect to Kubernetes: %s\n", err.Error())
	}

	dynamicClient, err := dynamic.NewForConfig(kubeConfig)
	if err != nil {
		return err
	}

	discoveryClient, err := discovery.NewDiscoveryClientForConfig(kubeConfig)
	if err != nil {
		return err
	}

	applyOptions := apply.NewApplyOptions(dynamicClient, discoveryClient)

	for _, iy := range installyaml {
		logging.Info("Applying yaml: %s", iy.Name)

		f, err := embedFiles.Open(iy.Location)
		if err != nil {
			return err
		}
		defer f.Close()

		b, err := ioutil.ReadAll(f)
		if err != nil {
			return err
		}

		yamlcontent := string(b)

		for key, value := range customValues {
			newValue, ok := value.(string)
			if !ok {
				continue
			}

			yamlcontent = strings.ReplaceAll(yamlcontent, key, newValue)
		}

		if err := applyOptions.Apply(context.Background(), []byte(yamlcontent)); err != nil {
			return err
		}
	}

	return nil
}
