const path = require('path')
const { slash } = require('gatsby-core-utils')

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions

  const docsTemplate = path.resolve('src/templates/docs.js')

  const result = await graphql(`
    {
      allMarkdownRemark(limit: 10000) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query`, result.errors)
    return
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.frontmatter.path,
      component: slash(docsTemplate)
    })
  })
}

// Implement the Gatsby API “onCreatePage”. This is
// called after every page is created.
exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions
  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/standard/)) {
    page.matchPath = "/standard/*"
    // Update the page.
    createPage(page)
  }
}
