---
title: "Addons"
path: "/docs/addons"
---

Middels addons is het eenvoudig mogelijk om een aantal basics te installeren op een Haven cluster. Gebruik van addons is optioneel en staat los van Haven Compliancy.

## Voorbeelden

### Monitoring

Inzicht op basis van de [prometheus-operator](https://github.com/prometheus-operator/kube-prometheus):

![Schermafbeelding van de Monitoring Addon](./schermafbeelding-monitoring-addon.png)

### Haven Dashboard

Het [Haven Dashboard](/docs/dashboard) is ook beschikbaar als addon:

![Schermafbeelding van de Haven Dashboard Addon](./schermafbeelding-haven-dashboard-addon.png)

De Haven CLI heeft het volledige overzicht van alle addons.

## Haven CLI

Addons zijn eenvoudig te beheren met de Haven CLI. De addons manager van de Haven CLI is feitelijk een frontend op [Helm](https://helm.sh/) met enkele definities. Dit geeft aansluiting op normerende technologie in plaats van zelf iets te bedenken.

Om bijvoorbeeld de monitoring addon te installeren kan het volgende commando gebruikt worden:

```bash
$ haven addons install monitoring
```

De gebruiker zal interactief worden gevraagd om vereiste informatie in te voeren, zoals bijvoorbeeld het internetadres waar de monitoring frontend bereikbaar op moet worden.

Voor meer informatie is er de ingebouwde help:

```bash
$ haven addons --help
```

&ensp;

---

*De Community addons inclusief het Haven Dashboard staan los van Haven Compliancy en zijn niet verplicht*
