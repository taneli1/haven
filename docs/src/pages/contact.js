import React from 'react'

import Layout from '../components/Layout'
import Seo from '../components/Seo'
import Container from '../components/Container'
import Section from '../components/Section'
import { ContactList, Icon } from '../components/ContactList'

import gitlab from '../components/ContactList/icons/gitlab.svg'
import mail from '../components/ContactList/icons/mail.svg'

const ContactPage = () => (
  <Layout>
    <Seo title="Contact" />
    <Container>
      <Section>
        <h1>Contact</h1>
        <p>Haven wordt ontwikkeld door <a href="https://www.vngrealisatie.nl/">VNG Realisatie</a> en maakt onderdeel uit van Common Ground.</p>
      </Section>
      <Section>
        <h2>Office Hours</h2>
        <p>Iedere woensdag tussen 10.00 en 11.00 is er een publieke <a href="https://meet.google.com/iio-rzvy-grs">Hangouts sessie</a> voor gemeenten, leveranciers en ontwikkelaars. Iedereen is van harte welkom om vragen te stellen en bij te praten.</p>
      </Section>
      <Section>
        <h2>Vragen over Haven?</h2>
        <p>Maak een issue aan op GitLab of stuur ons een e-mail.</p>

        <ContactList>
          <ContactList.Item>
            <Icon src={gitlab} />
            <a href="https://www.gitlab.com/commonground/haven/haven">gitlab.com</a>
          </ContactList.Item>
          <ContactList.Item>
            <Icon src={mail} />
            <a href="mailto:haven@vng.nl">haven@vng.nl</a>
          </ContactList.Item>
        </ContactList>
      </Section>
    </Container>
  </Layout>
)

export default ContactPage
